After cloning make sure you've got the ".env" file.
Now run "npm install" to install all modules
To run Backend run "npm start"

Routes:

Non-Protected:
/api/login/ 		POST	
/api/register/		POST
/api/test/		GET

Protected:
/api/contract/	            GET/POST/PUT/DELETE
/api/contract/accepted     GET                     All your accepted Contracts
/api/contract/accept/:id






