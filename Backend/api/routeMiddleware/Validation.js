const Joi = require('@hapi/joi');

const registerValidation = data => {
    const schema = Joi.object({
      password: Joi.string()
        .min(6)
        .required(),
      email: Joi.string()
        .min(6)
        .required()
        .email(),
      firstname: Joi.string()
        .required(),
      lastname: Joi.string()
        .required()
    });
    return schema.validate(data);
  };

  const loginValidation = data => {
      const schema = Joi.object({
        email: Joi.string()
         .min(6)
         .required()
         .email(),
        password: Joi.string()
        .min(6)
        .required(),
      });
      return schema.validate(data);
  }

  const contractValidation = data => {
    const schema = Joi.object({
      title: Joi.string().required().min(6),
      place: Joi.string().required(),
      time: Joi.date().required().timestamp(),
      form: Joi.string().required(),
      money: Joi.number().required(),
      music: Joi.string().required(),
      description: Joi.string().required(),
    });
    return schema.validate(data);
  }

module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
module.exports.contractValidation = contractValidation;
