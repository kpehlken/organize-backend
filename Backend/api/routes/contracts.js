const router = require('express').Router();
const authenticate = require('../routeMiddleware/verifyJwt');
const { contractValidation } = require('../routeMiddleware/Validation');
const Contract = require('../models/Contract');
const User = require('../models/User');
const jwt = require('jsonwebtoken');

router.post('/protected', authenticate, async (req, res) => {
    //let decodedTokenPayload = jwt.decode(req.headers['auth-token']);
    //let x = req.headers["authorization"].split(" ")[1];
    //res.send(jwt.decode(x));
    const retoken = req.body.refreshToken;
    
     let decoded = jwt.decode(retoken);
     
     //let decodedTokenPayload = jwt.decode(req.headers['authorization'].split(" ")[1]);

     
     const creator = await User.findOne({ _id: decoded._id });
     if(!creator) return res.status(400).send("Creator does not exists");
     res.send(creator);
});

router.put('/contract/cancel/:id', authenticate, async (req, res) => {
	Contract.findByIdAndUpdate({ _id: req.params.id}, { acceptedBy: null }).then(() => {
        Contract.findOne({ _id: req.params.id }).then((contract) => {
            res.send(contract);
        });
    });
});

router.get('/contract/accepted', authenticate, async (req, res) => {
	Contract.find({}).then((contracts) => {
		let decoded = jwt.decode(req.headers['authorization'].split(" ")[1]);
		

		let yourContracts = [];
		contracts.forEach((contract) => {
				if(contract.acceptedBy == decoded._id) {
                    			yourContracts.push(contract);
				}  
			});
		res.send(yourContracts);
	});
});

router.post('/contract', authenticate, async (req, res) => {

    //Validate Data before creating contract
    const { error } = contractValidation(req.body);
    if (error) return res.status(400).send(error.details[0].context["label"] + "-" + error.details[0].message);

    //Change Timestamp to readable format
    date = new Date(req.body.creationTime * 1000),
    datevalues = [
        date.getFullYear(),
        date.getMonth()+1,
        date.getDate(),
        date.getHours(),
        date.getMinutes(),
        date.getSeconds(),
    ];

    //Check if user with creatorId is existing and creatorEmail is equal

    let decodedTokenPayload = jwt.decode(req.headers['authorization'].split(" ")[1]);

    const creator = await User.findOne({ _id: decodedTokenPayload._id });
    if(!creator) return res.status(400).send("Creator does not exists");

    //Check if contract is unique
    /*
        ?
    */

   //Create Contract
   const contract = new Contract({
        creatorId: creator._id,
        creatorEmail: creator.email,
        creationTime: new Date().getTime(),
        title: req.body.title,
        place: req.body.place,
        time: req.body.time,
        money: req.body.money,
        description: req.body.description,
        form: req.body.form,
        music: req.body.music,
        acceptedBy: null
   });

   //Save to Database
   try {
       const savedContract = await contract.save();
       res.send(savedContract);
   } catch (err) {
       res.status(400).send(err);
   }
});

router.get('/contract', authenticate, (req, res) => {

    res.setHeader('Access-Control-Allow-Origin', '*');

    //List contracts
    Contract.find({}).then((contracts) => {
        res.send(contracts);
    });
});

router.put('/contract/accept/:id/', authenticate, async (req, res) => {
    //user is current user(FRONTEND)

    let decodedTokenPayload = jwt.decode(req.headers['authorization'].split(" ")[1]);

    const creator = await User.findOne({ _id: decodedTokenPayload._id });
    if(!creator) return res.status(400).send("Creator does not exists");

    //Update contract in db with accepted to true and acceptedBy to accepter
    Contract.findByIdAndUpdate({ _id: req.params.id}, { $set: { 'acceptedBy': creator._id } }).then(() => {
        Contract.findOne({ _id: req.params.id }).then((contract) => {
            res.send(contract);
        });
    });

    //Request to contract owner? 1E5c4K6REGdXFB4nu4PEyRFrgTuotN5ZhD
});

router.put('/contract/:id', authenticate, async (req, res) => {
    //Edit existing contract
    Contract.findByIdAndUpdate({ _id: req.params.id}, req.body).then(() => {
        Contract.findOne({ _id: req.params.id }).then((contract) => {
            res.send(contract);
        });
    });
});

router.delete('/contract/:id', authenticate, (req, res) => {
    //Delete existing contract
    Contract.findByIdAndDelete({ _id: req.params.id }).then((contract) => {
        res.send(contract);
    });
});

module.exports = router;
