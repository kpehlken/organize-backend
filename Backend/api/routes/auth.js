const express = require('express');
const router = express.Router();
const User = require('../models/User');
const { registerValidation, loginValidation } = require('../routeMiddleware/Validation');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const redis = require('redis');
const authenticate = require('../routeMiddleware/verifyJwt');

const client = redis.createClient();
client.auth(process.env.REDIS_PASSWORD);
client.on("error", (error) => {
    console.error(error);
});
client.on("connect", () => {
    console.log("[+] Connected to Redis");
});

router.post('/register', async (req, res) => {
    //Validate Data before creating new User
    const { error } = registerValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    //Check if User already exists in DB
    const emailExists = await User.findOne({email: req.body.email});
    if(emailExists) return res.status(400).json("Diese Email ist bereits registriert");
 
    //Hash Passwords
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    //Create User
    const user = new User({
        password: hashedPassword,
        email: req.body.email,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        status: "new",
        creationTime: new Date(Date.now()),
        createdContracts: 0,
        acceptedContracts: 0,
        activeContracts: 0,
        finishedContracts: 0
    });

    
    
    //Save to Database Maybe MONGODB REQUIRED
    try {
        const savedUser = await user.save();
        const token = jwt.sign({_id: savedUser._id}, process.env.TOKEN_SECRET, { expiresIn: '20m' });
        const refreshToken = jwt.sign({ _id: savedUser._id }, process.env.REFRESH_TOKEN_SECRET);
        res.json({
            authToken: token,
            refreshToken: refreshToken
        });
    } catch(err) {
        res.status(400).json(err);
    }
});

router.post('/login', async (req, res) => {
    //Validate Data before Logging in
    const { error } = loginValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    const expiration = 3600;

    //Checking if email exists
    const user = await User.findOne({ email: req.body.email });
    if(!user) return res.status(400).send("Es ist kein Benutzer mit dieser Email registriert");

    //Password check
    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if(!validPassword) return res.status(400).send("Ungültiger Benutzer oder ungültiges Passwort");

    //Create and assign a token
    const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET, { expiresIn: '20m' });
    const refreshToken = jwt.sign({ _id: user._id }, process.env.REFRESH_TOKEN_SECRET);

    const key = user._id.toString();
    //Store(push) refreshToken in Array or Database e.g. Redis Cache
    if(client.exists(key)) {
        client.del(key);
        client.setex(key, expiration, refreshToken);
    } else {
        client.setex(key, expiration, refreshToken);
    }
    
    res.json({
        authToken: token,
        refreshToken: refreshToken
    });
});

router.post('/token', (req, res) => {
    //Token is RefreshToken
    const refreshToken = req.body.refreshToken;
    let decodedTokenPayload = jwt.decode(refreshToken);

    if(!refreshToken) {
        return res.sendStatus(401);
    }
    //Instead of array.include decode jwt and get the token
    if(!client.exists(decodedTokenPayload._id)) {
        return res.sendStatus(404);
    }

    jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
        if(err) {
            return res.sendStatus(403);
        }
        const authToken = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET, { expiresIn: '20m' });

        res.json({
            token: authToken
        });
    });
});

router.delete('/logout', authenticate, (req, res) => {  
    const token = req.body.token;
    //Decode jwt to get RefreshToken content and use id as key for redis db and delete
    let decodedTokenPayload = jwt.decode(req.headers['authorization'].split(" ")[1]);
    if(client.get(decodedTokenPayload._id) != token) {
        res.send("Body and Database entry are not equal");
    }
    client.del(decodedTokenPayload._id);

    res.json({
        message: "Erfolgreich abgemeldet"
    });
});

router.get('/test', (req, res) => {
    //IMPORTANT BECAUSE OF * WILDCARD IT CANT SEND/RECIEVE COOKIES
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send({
        message: "Test route accessed"
    });
});

module.exports = router;