const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const authenticate = require('../routeMiddleware/verifyJwt');
const User = require('../models/User');

router.get('/user', authenticate, async (req, res) => {
    let token = req.headers.authorization.split(" ")[1];

    let payload = jwt.decode(token);

    const user = await User.findOne({ _id: payload._id });
    if(!user) { res.sendStatus(400).send("Kein Benutzer mit dieser ID gefunden"); }

    res.json({
        email: user.email,
        firstname: user.firstname,
        lastname: user.lastname,
        id: user._id,
        status: user.status,
        creationTime: user.creationTime,
        createdContracts: user.createdContracts,
        acceptedContracts: user.acceptedContracts,
        activeContracts: user.activeContracts,
        finishedContracts: user.finishedContracts,
    });
});

router.get('/user/:id', authenticate, async (req, res) => {

    const user = await User.findOne({ _id: req.params.id });
    if(!user) { res.send("Kein User mit dieser ID gefunden"); }

    User.findOne({ _id: req.params.id })
        .then((user) => {
            console.log(user._id);
            user.password = null;
            user.__v = null;

            res.send(user);

        }).catch((err) => {
            res.send(err);
        }); 
});

router.put('/user/:id', authenticate, async (req, res) => {

    const user = await User.findOne({ _id: req.params.id });
    if(!user) { res.sendStatus(400).send("Kein Benutzer mit dieser ID gefunden"); }

    User.findByIdAndUpdate({ _id: req.params.id}, req.body).then(() => {
        User.findOne({ _id: req.params.id }).then((user) => {
            res.send(user);
        });
    });
});

module.exports = router;