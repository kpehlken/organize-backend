const mongoose = require('mongoose');

const ContractSchema = mongoose.Schema({
    creatorId: {
        type: String,
        required: true
    },
    creatorEmail: {
        type: String,
        required: true
    },
    creationTime: {
        type: Date,
        required: true
    },
    title: {
      type: String,
      required: true
    },
    place: {
        type: String,
        required: true
    },
    time: {
        type: Date,
        required: true
    },
    money: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    form: {
        type: String,
        required: true
    },
    music: {
        type: String,
        required: true
    },
    acceptedBy: {
        type: String,
        required: false
    }
});

module.exports = mongoose.model('Contracts', ContractSchema);
