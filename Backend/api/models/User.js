const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String, 
        required: true
    },
    status: {       //Status: Active-, new-,  Member
        type: String,
        required: true
    },
    creationTime: {
        type: Date,
        required: true
    },
    createdContracts: {
        type: Number,
        required: false
    },
    acceptedContracts: {
        type: Number,
        required: false
    },
    activeContracts: {
        type: Number,
        required: false
    },
    finishedContracts: {
        type: Number,
        required: false
    }
});
//ADD following properties:
/*
//Add currently active contracts in Array 

status
creationTime
created Contracts

accepted Contracts

Active Contracts = visible on dashboard

finished Contracts


eventually nachweis/proof

*/

module.exports = mongoose.model('Users', UserSchema);