const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const redis = require('redis');
const socket = require('socket.io');

//dotenv
require("dotenv").config();

//Create Express server
let app = express();

//Redis Client
/*const client = redis.createClient();
client.auth(process.env.REDIS_PASSWORD);

client.on("error", (error) => {
    console.error(error);
});*/

//Connect to Database
mongoose.connect(process.env.DB_CONNECTION,
    { useNewUrlParser: true },
    () => console.log("[+] Connected to Database"),
);

//Alternative Cors middleware
/*
    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header(
            "Access-Control-Allow-Headers",
            "Origin, X-Requested-With, Content-Type, Accept, Authorization"
        );
        if(req.method === 'OPTIONS') {
            res.header('Access-Control-Allow-Methods', 'PUT, POST, DELETE');
            return res.status(200).json({});
        }
    });
*/

//Init Middleware
app.use(bodyParser.json());
app.use(cors()); 

//Import Routes
const authRoutes = require('./api/routes/auth');
const protectedRoutes = require('./api/routes/contracts');
const userRoutes = require('./api/routes/users');

//Routes
app.use('/api', authRoutes);
app.use('/api', protectedRoutes);
app.use('/api', userRoutes);

//Start Express Server
let server = app.listen(process.env.port || 5000, () => {
    console.log("[+] Server started on Port 5000 or " + process.env.port);
});

let io = socket(server);

io.on('connection', (socket) => {
    socket.on('chat', (data) => {
        socket.join(data.id);
        console.log("Joined")
        socket.to(data.id).emit('message', {
            message: data.message
        });
    })
});